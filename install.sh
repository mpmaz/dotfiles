#!/usr/bin/env bash

DOTFILES="$PWD"

pushd $HOME > /dev/null

opt_root=0
opt_deinstall=0

while getopts "rd" arg
do
	case $arg in
		r) opt_root=1;;
		d) opt_deinstall=1;;
	esac
done

function __append_line() {
	pushd $HOME > /dev/null
	LINE="$1"
	FILE="$2"

	if [ -f "$FILE.user" ]
	then
		FILE="$FILE.user"
	fi

	if [ $opt_deinstall -eq 0 ]
	then
		if [ ! -f "$FILE" ] || ! grep -q "$LINE" "$FILE"
		then
			mkdir -pv $(dirname "$FILE")
			echo adding \"$LINE\" to $FILE
			echo "$LINE" >> $FILE
		fi
	else
		if [ -f "$FILE" ] && grep -q "$LINE" "$FILE"
		then
			echo removing \"$LINE\" from $FILE
			sed -i "/$(echo $LINE | sed 's/\//\\\//g')/d" $FILE
		fi
	fi
	popd > /dev/null
}

function append_line() {
	__append_line "$1" "$2"

	if [ $opt_root -ne 0 ]
	then
		sudo cat << EOF | sudo bash -
opt_root=$opt_root
opt_deinstall=$opt_deinstall
$(declare -f __append_line)
__append_line "$(echo "$1" | sed -E 's/([\$"])/\\\1/g')" "$2"
EOF
	fi
}

function link_root() {
	pushd $HOME > /dev/null
	LINK="$(eval echo ~root)/$1"
	FILE="$HOME/$1"

	if [ $opt_root -eq 0 ]
	then
		return
	fi

	if [ $opt_deinstall -eq 0 ]
	then
		if ! sudo test -L "$LINK"
		then
			sudo mkdir -pv "$(dirname $LINK)"
			sudo ln -sv "$FILE" "$LINK"
		fi
	elif sudo test -L "$LINK"
	then
		sudo rm -vf "$LINK"
	fi
	popd > /dev/null
}

function git_clone() {
	pushd $HOME > /dev/null
	URL="$1"
	TAG="$2"
	DIR="$3"

	if [ $opt_deinstall -eq 0 ]
	then
		if [ ! -d "$DIR" ]
		then
			mkdir -pv $(dirname $DIR)
			git clone --depth 1 --branch "$TAG" "$URL" "$DIR"
		fi

		link_root "$DIR"
	elif [ -d "$DIR" ]
	then
		echo delete git repo $DIR
		rm -rf $DIR
	fi
	popd > /dev/null
}

function link_file() {
	pushd $HOME > /dev/null

	if [ $opt_deinstall -eq 0 ]
	then
		if ! test -L "$2"
		then
			mkdir -pv "$(dirname $2)"
			ln -sv "$1" "$2"
		fi
	elif test -L "$2"
	then
		rm -vf "$2"
	fi
	link_root "$2"
	popd > /dev/null
}

git_clone "https://github.com/tmux-plugins/tpm" "v3.1.0" ".config/tmux/plugins/tpm"
git_clone "https://github.com/dexpota/kitty-themes.git" "1.0.0" ".config/kitty/kitty-themes"

append_line "package.path = '$HOME/.config/envconf/?.lua'" ".config/nvim/init.lua"
append_line "require 'neovim'" ".config/nvim/init.lua"

append_line "source $DOTFILES/bashrc" ".bashrc"
append_line "\$include $DOTFILES/inputrc" ".inputrc"
append_line "source-file $DOTFILES/tmux.conf" ".config/tmux/tmux.conf"
append_line "source $DOTFILES/gdbinit" ".gdbinit"
append_line "include $DOTFILES/kitty.conf" ".config/kitty/kitty.conf"
append_line "include theme.conf" ".config/kitty/kitty.conf"
