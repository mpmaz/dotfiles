local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath("data")
	.. "/site/pack/packer/start/packer.nvim"
    if fn.empty(fn.glob(install_path)) > 0 then
	fn.system {
	    "git", "clone", "--depth", "1",
	    "https://github.com/wbthomason/packer.nvim",
	    install_path
	}
	vim.cmd [[packadd packer.nvim]]
	return true
    end
    return false
end

local packer_bootstrap = ensure_packer()

plugins = {
    theme = "marko-cerovac/material.nvim",
    tmux = "aserowy/tmux.nvim",
    nvimtree = {
	"nvim-tree/nvim-tree.lua",
	tag = "v1.2",
    },
    plenary = {
	"nvim-lua/plenary.nvim",
	tag = "v0.1.4",
    },
    devicons = "nvim-tree/nvim-web-devicons",
    nui = {
	"MunifTanjim/nui.nvim",
	tag = "0.3.0"
    },
    image = {
	"3rd/image.nvim",
	tag = "v1.1.0",
    },
    windowpicker = {
	"s1n7ax/nvim-window-picker",
	tag = "v2.0.2",
    },
    lualine = {
	"nvim-lualine/lualine.nvim",
    },
    winbar = {
	"fgheng/winbar.nvim",
    },
    telescope = {
	"nvim-telescope/telescope.nvim",
	tag = "0.1.6",
	requires = {
	    "nvim-lua/plenary.nvim",
	},
    },
    winshift = "sindrets/winshift.nvim",
    surround = {
	"kylechui/nvim-surround",
	tag = "v2.1.5",
    },
    undotree = {
	'mbbill/undotree',
	tag = "rel_6.1",
    },
    smartcolumn = {
	'm4xshen/smartcolumn.nvim',
	tag = "v1.1.1",
    },
    comment = {
	'numToStr/Comment.nvim',
	tag = "v0.8.0",
    },
    lspconfig = {
	'neovim/nvim-lspconfig',
	tag = "v0.1.6",
    },
    treesitter = {
	'nvim-treesitter/nvim-treesitter',
	tag = "v0.9.2",
    },
}

return require "packer".startup(function(use)
    use "wbthomason/packer.nvim"

    for dir, plugin in pairs(plugins) do
	use(plugin)
    end

    for dir, plugin in pairs(plugins) do
	pcall(require, "neovim.plugins." .. dir)
    end

    if packer_bootstrap then
	require "packer".sync()
    end
end)
