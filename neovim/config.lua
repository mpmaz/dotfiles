vim.g.c_syntax_for_h = true
vim.wo.number = true

vim.opt.wrap = false
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv('HOME') .. '/.undo/nvim'
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.scrolloff = 8
vim.opt.termguicolors = true
vim.opt.showcmd = true
vim.opt.autowrite = true
vim.opt.autoread = true
vim.opt.cursorline = false
vim.opt.tabstop = 8
vim.opt.shiftwidth = 4
vim.opt.expandtab = false
vim.opt.mouse = 'v'

vim.opt.autoread = true
vim.opt.autowrite = true
vim.opt.cinoptions='>s,Ls,:0,g0,t0,(0,u0,U1,w1,Ws,m1,j1,J1,P1'

vim.api.nvim_create_autocmd('FileType', {
    pattern = 'sh',
    callback = function()
	vim.opt.shiftwidth = 8
	vim.opt.smartindent = true
    end
})

vim.api.nvim_create_autocmd('FileType', {
    pattern = { 'c', 'lua' },
    callback = function()
	vim.opt.softtabstop = 4
	vim.opt.shiftwidth = 4
	vim.opt.cindent = true
    end
})
