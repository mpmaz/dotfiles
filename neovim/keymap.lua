vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
vim.g.undodir = '~/.var/nvim-undo'

vim.keymap.set('n', '<leader>t', ':tabe<CR>')
vim.keymap.set('n', '<leader>n', ':tabmove -1<CR>')
vim.keymap.set('n', '<leader>p', ':tabmove +1<CR>')
vim.keymap.set('n', '<M-q>', 'gT<CR>')
vim.keymap.set('n', '<M-w>', 'gt<CR>')
vim.keymap.set('i', '<C-å>', '<C-[>')

vim.keymap.set('n', '<leader>w', ':q<CR>')
vim.keymap.set('n', '<leader>W', ':q!<CR>')
vim.keymap.set('n', '<leader>r', ':so<CR>:PackerCompile<CR>')

vim.keymap.set('n', '<C-w>h', ':set nosplitright<CR>:vnew<CR>')
vim.keymap.set('n', '<C-w>j', ':set splitbelow<CR>:new<CR>')
vim.keymap.set('n', '<C-w>k', ':set nosplitbelow<CR>:new<CR>')
vim.keymap.set('n', '<C-w>l', ':set splitright<CR>:vnew<CR>')

vim.keymap.set('n', '<leader>s', function()
    vim.opt.hlsearch = not vim.o.hlsearch
end)

vim.keymap.set('n', '<C-u>', '<C-u>zz')
vim.keymap.set('n', '<C-d>', '<C-d>zz')
vim.keymap.set('n', '<leader>s', ':%s/')

vim.keymap.set('i', '<C-å>', '<C-[>')

vim.keymap.set('v', 'J', ":m '>+1<CR>gv=gv")
vim.keymap.set('v', 'K', ":m '<-2<CR>gv=gv")
