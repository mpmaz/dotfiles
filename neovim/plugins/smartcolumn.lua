require "smartcolumn".setup {
    colorcolumn = "80",
    disabled_filetypes = {
	"text", "help",
	"NvimTree", "lazy", "bash",
    }
}
