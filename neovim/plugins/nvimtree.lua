local treewidth = 30

require "nvim-tree".setup {
    view = {
	width = function()
	    return treewidth
	end,
	preserve_window_proportions = true,
    },
    modified = {
	enable = true,
    },
    filters = {
	git_ignored = false,
    },
    actions = {
	change_dir = {
	    global = true,
	},
	open_file = {
	    resize_window = false,
	},
    },
    tab = {
	sync = {
	    open = true,
	    close = true,
	},
    },
    ui = {
	confirm = {
	    default_yes = true,
	},
    },
    disable_netrw = true,
    sync_root_with_cwd = true,
    hijack_cursor = true,
}

local api = require "nvim-tree.api"

vim.keymap.set("n", "<leader>f", function()
    local win = vim.api.nvim_get_current_win()
    if api.tree.is_visible() then
	api.tree.focus()
	treewidth = vim.api.nvim_win_get_width(0)
	api.tree.close()
    else
	api.tree.open()
    end
    vim.api.nvim_set_current_win(win)
end)

vim.keymap.set("n", "F", function()
    if api.tree.is_visible() then
	api.tree.find_file()
    else
	api.tree.open {
	    find_file = true,
	}
    end
    api.tree.focus()
end)
