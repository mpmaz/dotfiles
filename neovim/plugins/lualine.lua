require "lualine".setup {
    options = {
	theme = "material",
	icons_enabled = true,
	globalstatus = true,
    },
    sections = {
	lualine_a = {
	    {
		"filename",
		path = 2,
	    }
	},
	lualine_b = {
	    "branch",
	    "diff",
	},
	lualine_c = {
	},
	lualine_x = {
	    "filetype"
	}
    },
    tabline = {}
}
