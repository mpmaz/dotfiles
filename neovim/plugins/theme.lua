require "material".setup {
    disable = {
	background = true
    },
}

vim.cmd.colorscheme "material"
vim.g.material_style = "deep ocean"
