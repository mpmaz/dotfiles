vim.keymap.set("n", "<leader>h", ":WinShift left<CR>")
vim.keymap.set("n", "<leader>j", ":WinShift down<CR>")
vim.keymap.set("n", "<leader>k", ":WinShift up<CR>")
vim.keymap.set("n", "<leader>l", ":WinShift right<CR>")
