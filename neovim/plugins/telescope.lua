local api = require("telescope.builtin")
local actions = require("telescope.actions")

require "telescope".setup {
}

vim.keymap.set("n", "<leader>i", function()
    api.find_files { hidden = false }
end)

vim.keymap.set("n", "<leader>I", function()
    api.find_files { hidden = true }
end)

vim.keymap.set("n", "<leader>o", api.oldfiles)
vim.keymap.set("n", "<leader>g", api.git_files)
vim.keymap.set("n", "<leader>H", api.help_tags)
