local tmux = require("tmux")

tmux.setup {
    navigation = {
	cycle_navigation = false,
	enable_default_keybindings = false,
    }
}

vim.keymap.set("n", "<M-h>", tmux.move_left)
vim.keymap.set("n", "<M-j>", tmux.move_bottom)
vim.keymap.set("n", "<M-k>", tmux.move_top)
vim.keymap.set("n", "<M-l>", tmux.move_right)

vim.keymap.set("n", "<M-H>", tmux.resize_left)
vim.keymap.set("n", "<M-J>", tmux.resize_bottom)
vim.keymap.set("n", "<M-K>", tmux.resize_top)
vim.keymap.set("n", "<M-L>", tmux.resize_right)
