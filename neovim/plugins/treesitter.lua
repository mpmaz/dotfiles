require "nvim-treesitter.configs".setup {
    ensure_installed = {
	"c", "lua", "bash",
	"meson", "make",
    },
    sync_install = false,
    auto_install = false,
    highlight = {
	enable = true
    }
}
