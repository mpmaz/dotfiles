export HISTCONTROL=ignoreboth:erasedups
export EDITOR=nvim

shopt -s autocd
shopt -u progcomp

PS1="\u@\h:\w> "
PROMPT_DIRTRIM=3

export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
export XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"

append() {
	local name="$1"
	shift
	for var in "$@"
	do
		if [ -d "$var" ] && [[ ":${!name}:" != *":$var:"* ]]
		then
			if [ -z "${!name}" ]
			then
				declare -g $name=$var
			else
				declare -g $name=${!name}:$var
			fi
		fi
	done
}

if [[ "${SHELLOPTS}" =~ :(emacs|vi): ]]
then
	bind '"\eu":"cd ..\n"'
fi

append PATH "$HOME/.local/bin" "/usr/local/bin"

for lib in $HOME/.local/*lib* /usr/local/*lib*
do
	append LD_LIBRARY_PATH $lib
done

select-kitty-theme() {
	pushd $HOME/.config/kitty/kitty-themes/themes
	conf=$(ls -1 *.conf | fzf)
	ln -svf $(pwd)/$conf $HOME/.config/kitty/theme.conf
	popd
}
